# alpine-dotnet-sdk
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-dotnet-sdk)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-dotnet-sdk)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-dotnet-sdk/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-dotnet-sdk/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [.NET Core](https://github.com/dotnet/core/)
    - .NET Core is a free and open-source managed computer software framework for the Windows, Linux, and macOS operating systems.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/alpine-dotnet-sdk:[ARCH_TAG]
```



----------------------------------------
#### Usage

* SDK



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

